package com.example.exchangeratesystem.service;

import com.example.exchangeratesystem.data.HistoricData;
import com.example.exchangeratesystem.exception.HistoricDataNotFoundException;
import com.example.exchangeratesystem.repository.HistoricDataRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ExchangeRateServiceTest {

    @Mock
    private HistoricDataRepository historicDataRepository;

    @InjectMocks
    ExchangeRateService exchangeRateService;

    private HistoricData historicData1 = new HistoricData(LocalDate.of(2020, 07, 12), getRatesData1());
    private HistoricData historicData2 = new HistoricData(LocalDate.of(2020, 07, 13), getRatesData2());

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        historicData1.setId(1L);
        historicData1.setId(2L);
    }

    @Test
    void allDataFromRepositoryAreReturnedSuccessfully() {

        given(historicDataRepository.findAll()).willReturn(Arrays.asList(historicData1, historicData2));

        List<HistoricData> rates = exchangeRateService.getAllRates();
        assertThat(rates, hasSize(2));
        verify(historicDataRepository, times(1)).findAll();
        assertFields(rates.get(0));
    }

    @Test
    void dataForDateIsReturnedSuccessfully() {

        given(historicDataRepository.findByDate(LocalDate.of(2020, 07, 12))).willReturn(Optional.of(historicData1));

        HistoricData historicData = exchangeRateService.getHistoricDataForDateAndSymbols("2020-07-12", Optional.empty());
        assertFields(historicData);
    }

    @Test
    void getDataForDateBeforeLoadThemThrowsException() {

        Throwable exception = assertThrows(HistoricDataNotFoundException.class, () ->
        {
            exchangeRateService.getHistoricDataForDateAndSymbols("2020-07-12", Optional.empty());
        });
        assertEquals("Data for date: 2020-07-12 not found", exception.getMessage());
    }

    @Test
    void dataForDateAndSymbolIsReturnedSuccessfully() {

        HashMap<String, BigDecimal> rates = new HashMap<>();
        rates.put("USD", BigDecimal.valueOf(1.131254));
        historicData1.setRates(rates);

        given(historicDataRepository.findByDateAndRatesCurrency(LocalDate.of(2020, 07, 12), List.of("USD"))).willReturn(Optional.of(historicData1));

        HistoricData dto = exchangeRateService.getHistoricDataForDateAndSymbols("2020-07-12", Optional.of("USD"));
        assertThat(dto.getRates()).hasSize(1);
        assertThat(dto.getRates()).containsKey("USD");
    }

    @Test
    void getDataForIncorrectDateThrowsException() {

        Throwable exception = assertThrows(DateTimeParseException.class, () ->
        {
            exchangeRateService.getHistoricDataForDateAndSymbols("2020-07-77", Optional.empty());
        });
        assertEquals("Text '2020-07-77' could not be parsed: Invalid value for DayOfMonth (valid values 1 - 28/31): " +
                "77", exception.getMessage());
    }

    @Test
    void getRatesForDateAndNonExistingSymbolThrowsException() {

        given(historicDataRepository.findByDateAndRatesCurrency(LocalDate.of(2020, 07, 12), List.of("USD"))).willReturn(Optional.of(historicData1));

        Throwable exception = assertThrows(HistoricDataNotFoundException.class, () ->
        {
            exchangeRateService.getHistoricDataForDateAndSymbols("2020-07-12", Optional.of("XYZ"));
        });
        assertEquals("Data for symbols: XYZ not found", exception.getMessage());
    }


    @Test
    void dataForDateRangeIsReturnedSuccessfully() {

        given(historicDataRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(LocalDate.of(2020, 07, 11),
                LocalDate.of(2020, 07, 12))).willReturn(List.of(historicData1, historicData2));

        List<HistoricData> rates = exchangeRateService.getRatesForDateRange("2020-07-11", "2020-07-12");
        assertThat(rates, hasSize(2));
        verify(historicDataRepository, times(1)).findAllByDateGreaterThanEqualAndDateLessThanEqual(LocalDate.of(2020,
                07, 11),
                LocalDate.of(2020, 07, 12));
        assertFields(rates.get(0));
    }

    private void assertFields(HistoricData historicDataDTO) {

        assertThat(historicDataDTO.getDate()).isInstanceOf(LocalDate.class);
        assertThat(historicDataDTO.getDate()).isEqualTo(LocalDate.of(2020, 07, 12));
        assertThat(historicDataDTO.getRates()).hasSize(3);
        assertThat(historicDataDTO.getRates().get("USD")).isInstanceOf(BigDecimal.class);
        assertThat(historicDataDTO.getRates().get("XYZ")).isNull();
    }

    private Map<String, BigDecimal> getRatesData1() {

        Map<String, BigDecimal> map = new HashMap<>();
        map.put("GBP", BigDecimal.valueOf(0.905371));
        map.put("USD", BigDecimal.valueOf(1.131254));
        map.put("HKD", BigDecimal.valueOf(8.768291));
        return map;
    }

    private Map<String, BigDecimal> getRatesData2() {

        Map<String, BigDecimal> map2 = new HashMap<>();
        map2.put("GBP", BigDecimal.valueOf(0.903271));
        map2.put("USD", BigDecimal.valueOf(2.131284));
        map2.put("HKD", BigDecimal.valueOf(1.734334));
        return map2;
    }
}