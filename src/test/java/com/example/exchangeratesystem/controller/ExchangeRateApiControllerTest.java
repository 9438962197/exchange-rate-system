package com.example.exchangeratesystem.controller;

import com.example.exchangeratesystem.data.HistoricData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ExchangeRateApiController.class)
class ExchangeRateApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExchangeRateApiController exchangeRateApiController;

    @Test
    void allRatesAreReturnedSuccessfully() throws Exception {

        when(exchangeRateApiController.getAllRates()).thenReturn(List.of(getFirstData(), getSecondData()));

        mockMvc.perform(get("/api/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[{\"date\":\"2020-07-12\",\"rates\":{\"HKD\":8.768291,\"GBP\":0.905371," +
                        "\"USD\":1.131254}},{\"date\":\"2020-07-11\",\"rates\":{\"HKD\":1.734334,\"GBP\":0.903271," +
                        "\"USD\":2.131284}}]"));
    }

    @Test
    void ratesForDateAreReturnedSuccessfully() throws Exception {

        when(exchangeRateApiController.getRateForDate("2020-07-12", Optional.empty())).thenReturn(getFirstData());

        mockMvc.perform(get("/api/2020-07-12"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"date\":\"2020-07-12\",\"rates\":{\"HKD\":8.768291,\"GBP\":0.905371," +
                        "\"USD\":1.131254}}"));
    }

    @Test
    void rateForDateAnsSymbolIsReturnedSuccessfully() throws Exception {

        when(exchangeRateApiController.getRateForDate("2020-07-12", Optional.of("USD"))).thenReturn(getDataWithUSDOnly());

        mockMvc.perform(get("/api/2020-07-12?symbols=USD"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"date\":\"2020-07-12\",\"rates\":{\"USD\":1.131254}}"));
    }

    @Test
    void ratesForDateRangeAreReturnedSuccessFully() throws Exception {

        when(exchangeRateApiController.getRatesForTimeRange("2020-07-11", "2020-07-12")).thenReturn(List.of(getFirstData(), getSecondData()));

        mockMvc.perform(get("/api/timeseries?start_date=2020-07-11&end_date=2020-07-12"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[{\"date\":\"2020-07-12\",\"rates\":{\"HKD\":8.768291,\"GBP\":0.905371," +
                        "\"USD\":1.131254}},{\"date\":\"2020-07-11\",\"rates\":{\"HKD\":1.734334,\"GBP\":0.903271," +
                        "\"USD\":2.131284}}]"));
    }

    private static HistoricData getFirstData() {
        HistoricData historicData = new HistoricData();
        historicData.setDate(LocalDate.of(2020, 07, 12));
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("GBP", BigDecimal.valueOf(0.905371));
        map.put("USD", BigDecimal.valueOf(1.131254));
        map.put("HKD", BigDecimal.valueOf(8.768291));
        historicData.setRates(map);
        return historicData;
    }

    private static HistoricData getDataWithUSDOnly() {
        HistoricData historicData = new HistoricData();
        historicData.setDate(LocalDate.of(2020, 07, 12));
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("USD", BigDecimal.valueOf(1.131254));
        historicData.setRates(map);
        return historicData;
    }

    private static HistoricData getSecondData() {
        HistoricData historicData2 = new HistoricData();
        historicData2.setDate(LocalDate.of(2020, 07, 11));
        Map<String, BigDecimal> map2 = new HashMap<>();
        map2.put("GBP", BigDecimal.valueOf(0.903271));
        map2.put("USD", BigDecimal.valueOf(2.131284));
        map2.put("HKD", BigDecimal.valueOf(1.734334));
        historicData2.setRates(map2);
        return historicData2;
    }
}
