package com.example.exchangeratesystem.controller;

import com.example.exchangeratesystem.service.ExternalExchangeRateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExternalExchangeRateApiController {

    private final ExternalExchangeRateService externalExchangeRateService;

    public ExternalExchangeRateApiController(ExternalExchangeRateService externalExchangeRateService) {
        this.externalExchangeRateService = externalExchangeRateService;
    }

    @GetMapping("/external")
    public void getExternalData() {
        externalExchangeRateService.fetchAllDataAndSave();
    }
}
