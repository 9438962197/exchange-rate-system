package com.example.exchangeratesystem.controller;

import com.example.exchangeratesystem.data.HistoricData;
import com.example.exchangeratesystem.service.ExchangeRateService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ExchangeRateApiController {

    private final ExchangeRateService exchangeRatesApiService;

    public ExchangeRateApiController(ExchangeRateService exchangeRatesApiService) {

        this.exchangeRatesApiService = exchangeRatesApiService;
    }

    @GetMapping("/all")
    public List<HistoricData> getAllRates() {
        return exchangeRatesApiService.getAllRates();
    }

    @GetMapping("/{date}")
    public HistoricData getRateForDate(@PathVariable String date, @RequestParam Optional<String> symbols) {
        return exchangeRatesApiService.getHistoricDataForDateAndSymbols(date, symbols);
    }

    @GetMapping("/timeseries")
    public List<HistoricData> getRatesForTimeRange(@RequestParam("start_date") String startDate,
                                                   @RequestParam("end_date") String endDate) {
        return exchangeRatesApiService.getRatesForDateRange(startDate, endDate);
    }
}
