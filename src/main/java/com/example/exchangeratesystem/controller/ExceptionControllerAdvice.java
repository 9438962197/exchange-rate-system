package com.example.exchangeratesystem.controller;

import com.example.exchangeratesystem.exception.HistoricDataNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.format.DateTimeParseException;

@ControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler({DateTimeParseException.class})
    public ResponseEntity dateTimeParseError(Exception ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incorrect date: " + ex.getMessage());
    }

    @ExceptionHandler({HistoricDataNotFoundException.class})
    public ResponseEntity dataNotFoundError(Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }
}
