package com.example.exchangeratesystem.repository;

import com.example.exchangeratesystem.data.HistoricData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface HistoricDataRepository extends JpaRepository<HistoricData, Long> {

    Optional<HistoricData> findByDate(LocalDate date);

    @Query("SELECT H FROM HistoricData H LEFT JOIN FETCH H.rates R where H.date = :date and KEY(R) in :symbols")
    Optional<HistoricData> findByDateAndRatesCurrency(LocalDate date, List<String> symbols);

    List<HistoricData> findAllByDateGreaterThanEqualAndDateLessThanEqual(LocalDate startDate, LocalDate endDate);
}
