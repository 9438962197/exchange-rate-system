package com.example.exchangeratesystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class HistoricDataNotFoundException extends RuntimeException{
    public HistoricDataNotFoundException(String message) {
        super(message);
    }
}
