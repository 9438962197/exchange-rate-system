package com.example.exchangeratesystem.service;

import com.example.exchangeratesystem.data.HistoricData;
import com.example.exchangeratesystem.exception.HistoricDataNotFoundException;
import com.example.exchangeratesystem.repository.HistoricDataRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExchangeRateService {

    private final HistoricDataRepository historicDataRepository;

    public ExchangeRateService(HistoricDataRepository historicDataRepository) {
        this.historicDataRepository = historicDataRepository;
    }

    public List<HistoricData> getAllRates() {
        List<HistoricData> allHistoricData = historicDataRepository.findAll();
        if (allHistoricData.isEmpty()) {
            throw new HistoricDataNotFoundException("Data not found");
        }
        return allHistoricData;
    }

    public HistoricData getHistoricDataForDateAndSymbols(String date, Optional<String> symbols) {
        Optional<HistoricData> historicData;
        LocalDate localDate = LocalDate.parse(date);
        if (symbols.isPresent()) {
            historicData = historicDataRepository.findByDateAndRatesCurrency(localDate,
                    Arrays.stream(symbols.get().split(",")).toList());
            return historicData.orElseThrow(() -> new HistoricDataNotFoundException("Data for symbols: " + symbols.get() + " not found"));
        } else {
            historicData = historicDataRepository.findByDate(localDate);
            return historicData.orElseThrow(() -> new HistoricDataNotFoundException("Data for date: " + date + " not " +
                    "found"));
        }
    }

    public List<HistoricData> getRatesForDateRange(String startDate, String endDate) {
        LocalDate localDateStartDate = LocalDate.parse(startDate);
        LocalDate localDateEndDate = LocalDate.parse(endDate);
        List<HistoricData> historicData =historicDataRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(localDateStartDate,
                        localDateEndDate);
        if (historicData.isEmpty()) {
            throw new HistoricDataNotFoundException("Data for range not found");
        }
        return historicData;
    }
}
