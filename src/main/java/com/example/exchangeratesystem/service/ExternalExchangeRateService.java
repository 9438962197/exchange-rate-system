package com.example.exchangeratesystem.service;

import com.example.exchangeratesystem.data.HistoricData;
import com.example.exchangeratesystem.repository.HistoricDataRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDate;
import java.time.Period;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExternalExchangeRateService {

    @Value("${api.key}")
    private String apiKey;

    @Value("${symbols}")
    private String symbols;

    private final HistoricDataRepository historicDataRepository;

    private final WebClient webClient = WebClient.create("http://api.exchangeratesapi.io");

    public ExternalExchangeRateService(HistoricDataRepository historicDataRepository) {
        this.historicDataRepository = historicDataRepository;
    }

    private Mono<HistoricData> getData(LocalDate date) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/v1/" + date)
                        .queryParam("access_key", apiKey)
                        .queryParam("symbols", symbols)
                        .build()
                )
                .retrieve()
                .bodyToMono(HistoricData.class);
    }

    private Flux<HistoricData> fetchAllDataAndSave(List<LocalDate> dates) {
        return Flux.fromIterable(dates)
                .parallel()
                .runOn(Schedulers.boundedElastic())
                .flatMap(this::getData)
                .sorted(Comparator.comparing(HistoricData::getDate));
    }

    public void fetchAllDataAndSave() {
        var allDatesToFetch = generateDatesToFetchData();
        Flux<HistoricData> historicDataDTOFlux = fetchAllDataAndSave(allDatesToFetch);
        List<HistoricData> collect = historicDataDTOFlux.collectList().block().stream().collect(Collectors.toList());
        historicDataRepository.deleteAll();
        historicDataRepository.saveAll(collect);
    }

    private List<LocalDate> generateDatesToFetchData() {
        LocalDate now = LocalDate.now();
        LocalDate lastYear = now.minusYears(1);
        return lastYear
                .datesUntil(now, Period.ofMonths(1))
                .map(localDate -> localDate.withDayOfMonth(1))
                .collect(Collectors.toList());
    }
}
