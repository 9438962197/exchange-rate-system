package com.example.exchangeratesystem.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

@Entity
public class HistoricData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private LocalDate date;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "rates", joinColumns = @JoinColumn(name = "historic_data_id"))
    @MapKeyColumn(name = "currency")
    @Column(name = "rate", precision = 12, scale = 6)
    private Map<String, BigDecimal> rates;

    public HistoricData() {
    }

    public HistoricData(LocalDate date, Map<String, BigDecimal> rates) {
        this.date = date;
        this.rates = rates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Map<String, BigDecimal> getRates() {
        return rates;
    }

    public void setRates(Map<String, BigDecimal> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "HistoricData{" +
                "id=" + id +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
