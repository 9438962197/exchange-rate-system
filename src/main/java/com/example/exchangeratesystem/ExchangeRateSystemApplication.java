package com.example.exchangeratesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ExchangeRateSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeRateSystemApplication.class, args);
    }
}
